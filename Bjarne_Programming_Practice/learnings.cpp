#include "learnings.h"
#include <iostream>
#include <string>

using namespace std;

void keep_window_open()
{
    char ch;
    cin >> ch;
}


/* Chapter 2*/
void ask_name()
{
    cout << "Please enter your first name:";
    string first_name;
    cin >> first_name;
    cout <<"Hello, "<< first_name <<"!\n"<<endl;
}

void double_practice()
{
    double grade;
    cout << "Enter your grade: ";
    cin >> grade;
    cout << grade << endl;
}

void get_name_age()
{
    string name;
    double age;


    //Get name first then age next
    cout << "What is your name: ";
    cin >> name;
    cout << "How old are you? (years): ";
    cin >> age;
    name += " [GM]";

    double agein_months = age * 12;

    cout << "Hello " << name << " your are " << agein_months << " months old."<<endl;
}

//Chapter 3
void c3_drill()
{
    // #1
    cout <<"Enter the name of the person you want to write to: ";
    string person_name;
    cin >> person_name;

    cout <<"How old is your friend ?";
    int age;
    while(cin >> age)
    {
        if(age == 0 || age == 110)
        {
            cout<<"You're kidding!\n";
            cout <<"How old is your friend ?";
        }
        else
        {
            break;
        }
    }

    cout <<"Enter other friend name: ";
    string other_name;
    cin >> other_name;

    cout <<"Enter other friend sex: (m) or (f)?: ";
    char gender;
    cin >> gender;

    // #1
    cout <<"Dear "<<person_name<<",\n";
    // #2
    cout<<"\tHow are you? I am fine. I miss you.\nBlah blah blah.\n";

    // #3
    cout <<"Have you seen "<<other_name<<" lately?\n";

    // #4
    if(gender == 'm')
    {
        cout <<"If you see "<<other_name<<" please ask him to call me.\n";
    }
    else
    {
        cout <<"If you see "<<other_name<<" please ask her to call me.\n";
    }

    // #5
    cout<<"I hear you just had a birthday and your "<<age<<" years old now.\n";

    // #6
    if(age < 12)
    {
        cout <<"Next year you will be "<<age + 1<<endl;
    }
    else if(age == 17)
    {
        cout <<"Next year you will be able to vote\n";
    }
    else if(age > 70)
    {
        cout<<"I hope you are enjoying retirement.\n";
    }

    // #7
    cout<<"\t\t\tYours sincerely,\n";
    cout<<"\t\t\t________________\n";
    cout<<"\t\t\tVincent Jay Bato\n";
}

void c3_exercises()
{
    //1,2
    /*  Miles to kilometers Converter
        1 mile = 1.609 kilometers
    */

    //#define E2
    #ifdef E2
    double data;
    cout<<"Total miles: ";
    cin>>data;
    double mk_ans = 1.609 * data;
    cout<<"There "<<mk_ans<<" kilometers to mile\n";
    #endif // E2

    //3

    //4
    #define E4
    #ifdef E4
    cout << "Enter two numbers: ";
    int a ,b;
    cin>>a>>b;
    if(a > b){
        cout<<a<<"is the largest number while "<<b<<" is the smallest.\n";
        cout<<"The difference of "<<a<<" and "<<b<<" = "<<a - b<<endl;
    }
    else{
        cout<<b<<"is the largest number while "<<a<<" is the smallest.\n";
        cout<<"The difference of "<<b<<" and "<<a<<" = "<<b - a<<endl;
    }
    cout<<"The sum of "<<a<<" and "<<b<<" = "<<a + b<<endl;
    cout<<"The product of "<<a<<" and "<<b<<" = "<<a * b<<endl;


    #endif // E4
}
