/*  Header files for all drills to be made from the book
    Programming Principles and Practice Using C++ - Bjarne Stroustrup

    Place all functions, class definitions here.
*/

void keep_window_open();

void ask_name();

void double_practice();

void get_name_age();

void c3_drill();

void c3_exercises();
